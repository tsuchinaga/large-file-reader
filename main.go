package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
)

func main() {
	// パラメータの読み取り
	var (
		filePathFlag = flag.String("f", "", "file path(string)")
		lineNumFlag  = flag.Int("n", 1, "read line(int)")
		linesFlag    = flag.Bool("l", false, "number of lines(bool)")
	)
	flag.Parse()

	filePath := *filePathFlag
	file, err := os.Open(filePath)
	if err != nil {
		fmt.Println(err)
		return
	}

	sc := bufio.NewScanner(file)
	sc.Split(bufio.ScanLines)

	n := 0 // 行数
	// 行数を数える
	if *linesFlag {
		for sc.Scan() {
			n++
		}
		fmt.Println("number of lines", n)
		return
	}

	stdin := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanLines)

	// n行スキップする
	for i := 1; i < *lineNumFlag; i++ {
		sc.Scan()
		n++
	}

	for sc.Scan() {
		n++
		fmt.Println("line", n, sc.Text())
		stdin.Scan()
	}
}
